Patches
=======

Boa parte do procedimento para criação de _patches_ está descrito [na própria
documentação de uso do Arcanist no
Enlightment](https://www.enlightenment.org/contrib/devs/arcanist-guide), mas de
toda forma, para simplificar (e de certa forma dar mais liberdade na descrição
do procedimento), ele está replicado aqui também.

Um resumo inicial para se saber é:
- O fluxo de _patches_ padrão do GitHub é **write**, **publish**, **review** e
  **merge**. Ou seja:
  - Modificações são feitas;
  - Um Pull/Merge Request (i.e. um _patch_) é feito para elas;
  - A equipe do projeto revisa;
  - Se passar na revisão, o _patch_ é aplicado ao _branch_ `master`.
- O fluxo de _patches_ padrão do Phabricator é **write**, **review**,
  **merge** e **publish**. Ou seja:
  - Modificações são feitas;
  - Um patch independente fica à disposição para revisão;
  - Após revisão, o patch é aplicado localmente;
  - Após aplicação, ele é publicado no `master`.

Se precisar de mais detalhes, cheque o [post sobre o fluxo de trabalho do
Phabricator](https://secure.phabricator.com/phame/post/view/766/write_review_merge_publish_phabricator_review_workflow/).
