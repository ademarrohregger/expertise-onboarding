Comunicação
===========

- [ ] **Phabricator**:
  - Cadastre-se em <https://phab.enlightenment.org/>:
    - Crie uma conta usando seu e-mail `@expertisesolutions.com.br`;
    - Verifique sua conta (o Phabricator irá mandar um e-mail com o link para
      isso).
- [ ] **IRC** (caso trabalhe na EFL/Enlightenment):
  - Instale um cliente IRC (ex: `hexchat` (GUI), `konversation` (GUI) ou
    `weechat` (terminal));
  - Conecte-se ao servidor do Freenode (irc.freenode.net);
  - Entre nos canais:
    - `#efl#`: Canal de comunicação sobre bindings de C# para a EFL.
    - `#e`: Canal de comunicação do Enlightenment
      (<https://www.enlightenment.org/contact>), usado principalmente para
      conversar com **usuários**;
    - `#edevelop`: Canal de comunicação de **desenvolvedores** do Enlightenment.

  Ex. com o `weechat`: na sua primeira conexão, execute:
  
  ```
    /server add freenode irc.freenode.net
  ```
  
  Nas seguintes conexões:
  
  ```
    /connect
    /join <canal que quer conectar>
  ```
  
  Também é possível automatizar o processo com:
  
  ```
    /set irc.server.freenode.autoconnect on
    /set irc.server.freenode.autojoin "#channel1,#channel2"
  ```
