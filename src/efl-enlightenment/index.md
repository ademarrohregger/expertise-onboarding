EFL/Enlightenment
=================

[**Enlightenment**](https://enlightenment.org) é um [Window
Manager](https://en.wikipedia.org/wiki/Window_manager) utilizado pela
**Samsung** para _mobile_, TV e Desktop.

> **Window Manager**
>
> Traduzindo literalmente, é um gerenciador de janelas - e o que ele faz
> efetivamente não é muito mais complexo que isso: programas gráficos vão ter
> janelas, componentes (botões, seções/painéis, barra de rolagem, além da
> própria janela), e alguém precisa ser responsável por desenhar cada um deles
> na tela, gerenciando também desde onde estão posicionados os componentes até
> os eventos que acontecem (cliques de mouse, pressionamento de teclas, e por
> aí vai).

[**EFL**](https://enlightenment.org/about-efl) (**E**lightenment **F**oundation
**L**ibraries) é um conjunto de bibliotecas gráficas que funciona da mesma
forma que Gtk e Qt e que compõe o Enlightenment.

O projeto em si é em cima da EFL, que possui uma _codebase_ bem grande
hospedada no Phabricator.

Quem está trabalhando em cima da EFL, no momento desta atualização, são:

- Felipe Almeida;
- João Paulo Tiz;
- Lucas Cavalcante de Sousa.

A codebase
----------

Como a _codebase_ da EFL é um tanto complexa, nesta seção fica um leve resumo
para dar um melhor direcionamento de onde olhar cada coisa.

> _(Nota: se a codebase real estiver muito diferente do que descrito aqui,
> então este documento está bem desatualizado e não deve servir de muito.
> Convém atualizá-lo se for o caso - e ser um novato no projeto é um bom
> momento para aproveitar e começar isso.)_

### Eolian

A EFL possui uma linguagem própria para descrever os componentes dela, chamada
"Eolian". Por padrão, arquivos com código Eolian usam a extensão .eo. A ideia é
que um código Eolian gere uma API **C#** que chame funções nativas e instancie
objetos nativos, todos descritos em **C**, relacionados aos componentes
descritos nos .eo's. Essa API é fornecida tanto para .NET quanto para Mono
(dois ambientes de execução para os quais C# é direcionado).

Por ser uma linguagem da EFL, a parte relacionada a Eolian da _codebase_
contém:
- Uma mini-biblioteca em **C++** que para **parsear** código Eo e gerar uma
  **abstração** das propriedades;
- Uma mini-biblioteca, também em **C++**, de **geração** de código;

A parte da EFL que lida com Eolian para C# fica em `src/bin/eolian_mono`.
