Ferramentas
===========

Build
-----

Você pode descobrir as ferramentas de _build_ pelo [tutorial de instalação da
EFL](https://git.enlightenment.org/core/efl.git/tree/INSTALL), mas para
facilitar sua vida, seguem elas listadas direitinho:

- **[Meson](https://mesonbuild.com/):** é um sistema de _build_ que separa
  diferentes configurações de _builds_ para diferentes diretórios em arquivos
  `meson.build`, com as configurações descritas em uma linguagem própria do
  Meson e que é bastante semelhante.
- **[Ninja](https://github.com/ninja-build/ninja/):** é um sistema de _build_
  que o Meson usa. Enquanto o Meson vai gerar rotinas de compilação e etc. para
  cada configuração de _build_ desejada (ex: uma _build_ para Mono, outra para
  Dotnet, outra para releases...), o Ninja irá chamar as rotinas de compilação
  em si para uma determinada configuração de _build_.

É necessário instalar apenas o Meson, afinal o Ninja é dependência dele. Você
pode ver na documentação oficial [como instalar o
Meson](https://mesonbuild.com/Getting-meson.html), mas provavelmente ele tem no
sistema de pacotes da sua distribuição:

```console
# pacman -S meson   # ArchLinux/Arch-based (Manjaro, EndeavourOS, ...)
# apt install meson # Debian/Debian-based (Ubuntu, Mint, ...)
```

Ou direto pelo Pip (Meson e Ninja são projetos Python):

```console
$ pip install --user meson  # Utilize pip3 se sua distro usar Python2 como padrão
```

Dependências do projeto
-----------------------

Na hora de buildar, é bem provável que dêem erros por não ter algumas
bibliotecas. Instalando o pacote delas já é o suficiente, mas como vai aparecer
apenas uma por vez, segue uma lista resumida delas:
- `scim`;
- `poppler`;
- `check`;
- `libspectre`.

Se estiver em um Arch, é legitimamente jogar os 4 pacotes em um único comando:

```console
# pacman -S scim poppler check libspectre
```

Patches
-------

Para submeter _patches_ (caso esteja em um projeto _hosteado_ no Phabricator),
é utilizado o **Arcanist**.

### Instalação

Você pode seguir o [guia de instalação do
Arcanist](https://secure.phabricator.com/book/phabricator/article/arcanist/#installing-arcanist).

Mais uma vez, no ArchLinux, você pode usar algum AUR-helper (`yay`, `pikaur`,
...):

```console
# pikaur -S arcanist
```

Após a instalação, você precisa inicializar o Arcanist com uma _**API-key**_
gerada pelo Phabricator.  O link para isso é informado pelo próprio Arcanist
quando você tentar usá-lo, então jogue um `arc feature` ou qualquer coisa do
tipo, leia a mensagem de erro e siga os passos dela.

### Uso

Um pequeno _cheatsheet_ do Arcanist para montar um _workflow_ interessante:

#### Comandos úteis para começar uma task

| Comando                        | Descrição                                                                                    |
| ------------------------------ | -------------------------------------------------------------------------------------------- |
| `arc browse <obj>`             | Abre a página de um objeto do Phabricator no navegador.                                      |
| `arc feature`                  | Mostra os _branches_ e Diffs, indicando o status da revisão (ex: "Needs Review", "Closed").  |
| `arc feature <name>`           | Cria/Entra no _branch_ `<name>`.                                                             |
| `arc list`                     | Lista os seus Diffs que ainda estão abertos.                                                 |
| `arc tasks`                    | Mostra as _tasks_ que foram estabelecidas a você no Phabricator.                             |
| `arc todo <sumário>`           | Cria uma _task_ para você mesmo.                                                             |
| `arc todo <sumário> --browse`  | Cria uma _task_ para você mesmo e abre ela no navegador após a criação.                      |

#### Comandos úteis para o dia-a-dia

| Comando                        | Descrição                                                                          |
| ------------------------------ | ---------------------------------------------------------------------------------- |
| `arc diff <commit>`            | Gera um Diffusion a partir do `<commit>` (**Dica:** `HEAD~` e `HEAD~N` funcionam). |
| `arc flag`                     | Lista as flags criadas em commits ou objetos do Phabricator (Diffs, etc.).         |
| `arc flag <obj> --clear`       | Limpa as flags de um objeto.                                                       |
| `arc flag <obj> --note <nota>` | Adiciona uma nota para um objeto.                                                  |
| `arc start <obj>`              | Começa a monitorar o tempo de trabalho em um objeto (ex: um Diff `DXXXX`).         |
| `arc stop <obj>`               | Para de monitorar o tempo de trabalho em um objeto (ex: um Diff `DXXXX`).          |
| `arc time`                     | Mostra sobre qual objeto você está monitorando o tempo.                            |

#### Comandos úteis para review

| Comando                        | Descrição                                        |
| ------------------------------ | ------------------------------------------------ |
| `arc patch <diff>`             | Aplica um Diff (criando um novo _branch_).       |
| `arc patch --nobranch <diff>`  | Aplica um Diff (**sem criar** um novo _branch_). |

Dependências adicionais
-----------------------

**(TODO: verificar se isso é realmente necessário)** Ao gerar o sistema de
build com `meson <diretório de build> <configs de build>`, procure nos logs por
"dependency not found" ou semelhantes caso queira se certificar de que está
cobrindo boa parte da ferramenta na hora de ver se suas modificações não
introduziram _breaking changes_.

Algumas dependências adicionais:
- RLottie: formato de arquivo de animações;
- CMake: sistema de build para C e C++;
- Doxygen: gerador de documentação.
- Valgrind: verifica problemas de gerenciamento de memória;
