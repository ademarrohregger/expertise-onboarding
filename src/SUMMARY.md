# Summary

- [Preparações](./preparacoes.md)
- [Fluxo de Trabalho](./fluxo-de-trabalho/index.md)
    - [Comunicação Interna](./fluxo-de-trabalho/comunicacao.md)
    - [Dia a dia e Infraestrutura](./fluxo-de-trabalho/dia-a-dia.md)
    - [Projetos](./fluxo-de-trabalho/projetos.md)
- [EFL/Enlightenment](./efl-enlightenment/index.md)
    - [Ferramentas](./efl-enlightenment/ferramentas.md)
    - [Comunicação](./efl-enlightenment/comunicacao.md)
    - [Patches](./efl-enlightenment/patches.md)
        - [Patches Novos](./efl-enlightenment/patches-novos.md)
        - [Dia-a-dia](./efl-enlightenment/dia-a-dia.md)
        - [Atualizando Patches](./efl-enlightenment/atualizando-patches.md)
