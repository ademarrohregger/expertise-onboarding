Internos
========

Dailies
-------

A ideia das dailies é simples: todo dia, às 11h, mandar uma mensagem no
[#daily](https://expertisesolutions.slack.com/channel/daily) com:
- O que foi feito no dia anterior;
- O que será feito no dia de hoje.

Infraestrutura
==============

Geral
-----

Na Expertise utilizamos [**Git**](https://git-scm.com) para versionamento.

Servidor
--------

**(TODO)**
