Fluxo de Trabalho
=================

Para resumir bem resumido, no dia-a-dia:

### Comunicação interna

- Nos comunicamos (enquanto equipe local) via **Slack**, mantendo contato em
  uma frequência razoável discutindo mudanças nos códigos, decisões, e por aí
  vai.

### Comunicação por projeto

- Dependendo do projeto, temos meios de comunicação específicos para eles:
  - Por exemplo: para o **EFL/Enlightment**, conversamos com pessoas de fora do
    Brasil pelo **Phabricator** e pelo **IRC**.

### Base de código

- Mantemos o histórico trabalho em repositórios git no **GitOLite** (semelhante
  a um servidor GitLab na nuvem).
